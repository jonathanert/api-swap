<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class NodinController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //get data dari nodim
    function index_get(){
        $nodin_id = $this->get('nodim_id');
        $limit = $this->get('limit');
        $page  = $this->get('page');
        if ($nodin_id == '') {
            $this->db->order_by('NODIN_ID','DESC');
            $getnodim = $this->db->get('t_nodin_swap',$limit,$page)->result();
        } else {
            $this->db->where('NODIN_ID', $nodin_id);
            $getnodim = $this->db->get('t_nodin_swap',$limit,$page)->result();
        }
        $this->response($getnodim, 200);
    }

    //update data ke nodim
    /*function index_put(){

        $puted = $this->put();
        $siteid = $this->put('siteid');

        for($i=0; $i < count($siteid); $i++){
            $data = array(
                'siteid'            => $puted['siteid'][$i],
                'ne_id'             => $puted['ne_id'][$i],
                'sector_id'         => $puted['sector_id'][$i],
                'site_name'         => $puted['site_name'][$i],
                'oss_name'          => $puted['oss_name'][$i],
                'kabupaten'         => $puted['kabupaten'][$i],
                'lac'               => $puted['lac'][$i],
                'cell_name'         => $puted['cell_name'][$i],
                'sac'               => $puted['sac'][$i],
                'scrambling_code'   => $puted['scrambling_code'][$i],
                'rnc'               => $puted['rnc'][$i],
                'rnc_id'            => $puted['rnc_id'][$i],
                'rac'               => $puted['rac'][$i],
                'ura_id'            => $puted['ura_id'][$i],
                'mscs_name'         => $puted['mscs_name'][$i],
                'mscs_spc'          => $puted['mscs_spc'][$i],
                'mgw_name'          => $puted['mgw_name'][$i],
                'mgw_spc'           => $puted['mgw_spc'][$i],
                'locno'             => $puted['locno'][$i],
                'poc_pstn'          => $puted['poc_pstn'][$i],
                'time_zone'         => $puted['time_zone'][$i],
                'sow'               => $puted['sow'][$i],
                'longitude'         => $puted['longitude'][$i],
                'latitude'          => $puted['latitude'][$i]
            );

            $this->db->where('siteid',$siteid[$i]);
            $update = $this->db->update('nodin', $data);

            if($update){
                $this->response($data,200);
            }else{
                $this->response(array('status' => 'fail', 502));
            }
        }
    }
    */
    function index_post(){
      $posted = $this->post();
      $cell_name = $this->post('CELL_NAME');

      for($i=0; $i < count($cell_name); $i++){
          $datas = array(
            'LAC/TAC_new' => $posted['LAC/TAC_new'],
            'CI_new'      => $posted['CI_new'],
          );

          $this->db->where('CELL_NAME',$cell_name);
          $update = $this->db->update('t_plan_neid', $datas);

          if($update){
              $this->response($datas,200);
          }else{
              $this->response(array('status' => 'fail', 502));
          }
      }
    }

    //update data ke nodim
    function index_put(){

        $puted = $this->put();
        $cell_name = $this->put('CELL_NAME');

        for($i=0; $i < count($cell_name); $i++){
            if ($puted['BAND'] == '4G') {
              $data = array(
                'CELL_NAME' => $puted['CELL_NAME'],
                'VENDOR'    => $puted['VENDOR'],
                'REGIONAL'  => $puted['REGIONAL'],
                'TAC'       => $puted['TAC'],
                'CI'        => $puted['CI'],
                'NEW_TAC'   => $puted['NEW_TAC'],
                'NEW_CI'    => $puted['NEW_CI'],
                'SITE_ID'   => $puted['SITE_ID'],
                'NE_ID'     => $puted['NE_ID'],
                'BAND'          => $puted['BAND'],
                'DESCRIPTION_OF_CHANGE' => $puted['DESCRIPTION_OF_CHANGE'],
                'REASON_FOR_CHANGE'         => $puted['REASON_FOR_CHANGE'],
                'EXECUTOR_PIC_COMPANY'  => $puted['EXECUTOR_PIC_COMPANY'],
                'EXECUTOR_PIC_NAME'         => $puted['EXECUTOR_PIC_NAME'],
                'EXECUTOR_PIC_PHONE'        => $puted['EXECUTOR_PIC_PHONE'],
                'EXECUTOR_PIC_EMAIL'        => $puted['EXECUTOR_PIC_EMAIL'],
                'NETWORLELEMENT_NAME'       => $puted['NETWORLELEMENT_NAME'],
                'USER_ACCESS_LOGIN'         => $puted['USER_ACCESS_LOGIN'],
                'NE_TYPE'                               => $puted['NE_TYPE'],
                'AREA_NE'                               => $puted['AREA_NE'],
                'ADDITIONAL_INFORMATION'=> $puted['ADDITIONAL_INFORMATION'],
                'ACCESS_LEVEL'                  => $puted['ACCESS_LEVEL'],
                'ROLEKEYS'                          => $puted['ROLEKEYS'],
                'RESPONSIBLE_APPROVAL'  => $puted['RESPONSIBLE_APPROVAL'],
                'DETAILED_LOCATION'         => $puted['DETAILED_LOCATION'],
                'NE_NAME_IMPACTED_LIST' => $puted['NE_NAME_IMPACTED_LIST'],
                'NE_IMPACT_DESCRIPTION' => $puted['NE_IMPACT_DESCRIPTION'],
                'NE_OUTAGE_DURATION_MIN'=> $puted['NE_OUTAGE_DURATION_MIN'],
                'AFFECTED_SERVICE_DESCRIPTION' => $puted['AFFECTED_SERVICE_DESCRIPTION'],
                'SEVICE_OUTAGE_DURATION_MIN'     => $puted['SEVICE_OUTAGE_DURATION_MIN'],
                'NE_ID_PRIMARY'                 => $puted['NE_ID_PRIMARY'],
                'NE_NAME_PRIMARY'               => $puted['NE_NAME_PRIMARY'],
                'BTS_NAME'                          => $puted['BTS_NAME'],
                'FREQUENCY'                         => $puted['FREQUENCY'],
                'AFFECTED_SERVICE'      => $puted['AFFECTED_SERVICE']
              );
            }else{
              $data = array(
                'CELL_NAME' => $puted['CELL_NAME'],
                'VENDOR'    => $puted['VENDOR'],
                'REGIONAL'  => $puted['REGIONAL'],
                'LAC'       => $puted['LAC'],
                'CI'        => $puted['CI'],
                'NEW_LAC'   => $puted['NEW_LAC'],
                'NEW_CI'    => $puted['NEW_CI'],
                'SITE_ID'   => $puted['SITE_ID'],
                'NE_ID'     => $puted['NE_ID'],
                'BAND'          => $puted['BAND'],
                'DESCRIPTION_OF_CHANGE' => $puted['DESCRIPTION_OF_CHANGE'],
                'REASON_FOR_CHANGE'         => $puted['REASON_FOR_CHANGE'],
                'EXECUTOR_PIC_COMPANY'  => $puted['EXECUTOR_PIC_COMPANY'],
                'EXECUTOR_PIC_NAME'         => $puted['EXECUTOR_PIC_NAME'],
                'EXECUTOR_PIC_PHONE'        => $puted['EXECUTOR_PIC_PHONE'],
                'EXECUTOR_PIC_EMAIL'        => $puted['EXECUTOR_PIC_EMAIL'],
                'NETWORLELEMENT_NAME'       => $puted['NETWORLELEMENT_NAME'],
                'USER_ACCESS_LOGIN'         => $puted['USER_ACCESS_LOGIN'],
                'NE_TYPE'                               => $puted['NE_TYPE'],
                'AREA_NE'                               => $puted['AREA_NE'],
                'ADDITIONAL_INFORMATION'=> $puted['ADDITIONAL_INFORMATION'],
                'ACCESS_LEVEL'                  => $puted['ACCESS_LEVEL'],
                'ROLEKEYS'                          => $puted['ROLEKEYS'],
                'RESPONSIBLE_APPROVAL'  => $puted['RESPONSIBLE_APPROVAL'],
                'DETAILED_LOCATION'         => $puted['DETAILED_LOCATION'],
                'NE_NAME_IMPACTED_LIST' => $puted['NE_NAME_IMPACTED_LIST'],
                'NE_IMPACT_DESCRIPTION' => $puted['NE_IMPACT_DESCRIPTION'],
                'NE_OUTAGE_DURATION_MIN'=> $puted['NE_OUTAGE_DURATION_MIN'],
                'AFFECTED_SERVICE_DESCRIPTION' => $puted['AFFECTED_SERVICE_DESCRIPTION'],
                'SEVICE_OUTAGE_DURATION_MIN'     => $puted['SEVICE_OUTAGE_DURATION_MIN'],
                'NE_ID_PRIMARY'                 => $puted['NE_ID_PRIMARY'],
                'NE_NAME_PRIMARY'               => $puted['NE_NAME_PRIMARY'],
                'BTS_NAME'                          => $puted['BTS_NAME'],
                'FREQUENCY'                         => $puted['FREQUENCY'],
                'AFFECTED_SERVICE'      => $puted['AFFECTED_SERVICE']
                    );
            }

            $this->db->where('CELL_NAME',$cell_name);
            $update = $this->db->update('t_nodin_swap', $data);

            if($update){
                $this->response($data,200);
            }else{
                $this->response(array('status' => 'fail', 502));
            }
        }
    }
}
?>
