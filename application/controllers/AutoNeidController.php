<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class AutoNeidController extends REST_Controller {

    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    public function index_get()
    {
        $ne_id = $this->get('ne_id');
        $this->db->distinct();
        if ($ne_id != '') {
            $this->db->like('NE_ID', $ne_id);
        }
        $this->db->select('NE_ID');
        $getnodin = $this->db->get('t_nodin_swap')->result();
        $this->response($getnodin, 200);
    }

    public function index_post()
    {

    }

    public function index_put()
    {

    }

}
