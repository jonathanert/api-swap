<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class NoNodinController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //get data dari no_nodim
    //get data dari nodim
    function index_get(){
        $nodin_id = $this->get('nodim_id');
        $limit = $this->get('limit');
        $page  = $this->get('page');
        if ($nodin_id == '') {
            $this->db->where('NODIN_ID !=',NULL);
            $this->db->group_by('NODIN_ID');
            $this->db->select('NODIN_ID,CRQ,INPUT_DATE');
            $getnodim = $this->db->get('t_nodin_swap',$limit,$page)->result();
        } else {
            $this->db->distinct();
            $this->db->select('NODIN_ID,CRQ,INPUT_DATE');
            $this->db->where('NODIN_ID', $nodin_id);
            $getnodim = $this->db->get('t_nodin_swap',$limit,$page)->result();
        }
        $this->response($getnodim, 200);
    }

    function index_post(){
        $data = array(
            'NODIN_ID'  => $this->input->post('NODIN_ID'),
            'KEPADA'    => $this->input->post('KEPADA'),
            'DARI'      => $this->input->post('DARI'),
            'LAMPIRAN'  => $this->input->post('LAMPIRAN'),
            'PERIHAL'   => $this->input->post('PERIHAL'),
            'KONTEN'    => $this->input->post('KONTEN')
        );

        $insert = $this->db->insert('t_export_nodin_swap',$data);
        if($insert) {
            $this->response($data,200);
        } else{
            $this->response(array('status' => fail, 502));
        }
    }

    function index_put(){
        $no_nodin = "";
        $bulan = substr(date('Y-m-d'), 5, 2);
        $tahun = substr(date('Y-m-d'), 0,4);

        $data_nonodin = $this->db->get('t_no_nodin_swap')->result();
        foreach ($data_nonodin as $no_nodin) {
            if($no_nodin->MONTH != $bulan){
                $month = array(
                  'month' => $bulan
                );
                $this->db->where('NO',$no_nodin->NO);
                $this->db->update('t_no_nodin_swap',$month);

                $date = array(
                    'no' => 1
                );
                $this->db->where('MONTH',$bulan);
                $this->db->update('t_no_nodin_swap',$date);
            } else{
                $data = array(
                    'no' => $no_nodin->NO + 1
                );

                $this->db->where('MONTH',$bulan);
                $this->db->update('t_no_nodin_swap',$data);
            }

            $data_new = $this->db->get('t_no_nodin_swap')->result();

            foreach ($data_new as $no_nodin_new) {
              switch ($no_nodin_new->MONTH) {
                  case 1:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/I/".$tahun;
                      break;
                  case 2:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/II/".$tahun;
                      break;
                  case 3:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/III/".$tahun;
                      break;
                  case 4:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/IV/".$tahun;
                      break;
                  case 5:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/V/".$tahun;
                      break;
                  case 6:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/VI/".$tahun;
                      break;
                  case 7:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/VII/".$tahun;
                      break;
                  case 8:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/VIII/".$tahun;
                      break;
                  case 9:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/IX/".$tahun;
                      break;
                  case 10:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/X/".$tahun;
                      break;
                  case 11:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/XI/".$tahun;
                      break;
                  case 12:
                      $no_nodin = str_pad($no_nodin_new->NO,3,'0',STR_PAD_LEFT)."/TC.01/RI-21/XII/".$tahun;
                      break;
                  default:
                      $no_nodin = "Not Found";
                      break;
              }
            }

            $flag_nodin = $this->put('FLAG_NODIN');
            $data = array(
                'NODIN_ID'    => $no_nodin,
                'DESCRIPTION' => $this->put('DESCRIPTION'),
                'swap'  => $this->put('swap'),
                'FLAG_NODIN'  => $flag_nodin
            );

            $this->db->where('FLAG_NODIN',1); // filter untuk dapat nodin id dan sudah dapet CRQ
            $update = $this->db->update('t_nodin_swap',$data);

            $new_neid = array(
              'NE_ID_new' =>$this->put('swap')
            );

            $this->db->where('CELL_NAME',$cell_name);
            $set_neid = $this->db->update('t_plan_neid',$new_neid);

            // $tiket = array(
            //   'id_ticket'     => "DISMANTLE/".date('Ymd')."/".str_pad($no_nodin->NO,3,'0',STR_PAD_LEFT),
            //   'submitted_by'  => "Rizki_Adrianto",
            //   'waiting_for'   => "Dwi_SG_Utomo",
            //   'data'          => json($this->db->get('t_nodin_swap')->result()),
            //   'counter'       => 3
            // );
            //
            // $history = array(
            //   'id_ticket'  => "DISMANTLE/".date('Ymd')."/".str_pad($no_nodin->NO,3,'0',STR_PAD_LEFT),
            //   'action_by'  => "Rizki_Adrianto",
            //   'manager'    => "Dwi_SG_Utomo",
            //   'action'     => "Submit"
            // );
            //
            // $db2 = $this->load->database('otherdb', TRUE);
            // $workflowTiket = $db2->insert('tiket',$tiket);
            // $workflowHistory = $db2->insert('hisotry_workflow',$history);

            if($update) {
                $this->response($data,200);
            } else{
                $this->response(array('status' => 'fail',502));
            }
        }
    }
}
?>
