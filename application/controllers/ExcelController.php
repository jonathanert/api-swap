<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ExcelController extends REST_Controller {

    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    public function index_get()
    {
        $ne_id = $this->get('ne_id');
        if ($ne_id != '') {
            $this->db->where('NE_ID', $ne_id);
        }
        $getnodin = $this->db->get('t_nodin_swap')->result();
        $this->response($getnodin, 200);
    }

    public function index_post()
    {

    }

    public function index_put()
    {
        $put = $this->put();

        $data = [
            'REGIONAL' => $put['REGIONAL'],
            'VENDOR' => $put['VENDOR'],
            'CELL_NAME' => $put['CELL_NAME'],
            'CI' => $put['CI'],
            'NE_ID' => $put['NE_ID'],
            'SITE_ID' => $put['SITE_ID'],
            'NODIN_ID' => $put['NODIN_ID'],
            'NODEB_NAME' => $put['NODEB_NAME'],
            'BAND' => $put['BAND'],
            'NEW_CI' => $put['NEW_CI'],
            'DESCRIPTION' => $put['DESCRIPTION'],
            'STATUS' => $put['STATUS'],
            'SWAP' => $put['SWAP']
        ];
        if ($put['BAND'] == '2G' || $put['BAND'] == '3G') {
            $data['LAC'] = $put['LAC_TAC'];
            $data['NEW_LAC'] = $put['NEW_LAC_TAC'];
        }elseif ($put['BAND'] == '4G') {
            $data['TAC'] = $put['LAC_TAC'];
            $data['NEW_TAC'] = $put['NEW_LAC_TAC'];
        }

        $this->db->where('CELL_NAME', $data['CELL_NAME']);
        $update = $this->db->update('t_nodin_swap', $data);

        if($update){
            $this->response('Success', 200);
        }else{
            $this->response(array('status' => 'fail', 502));
        }
    }

}
