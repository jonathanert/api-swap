<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class GetNoController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data dafinci-po
    function index_get() {

      $get_nodin = $this->db->get('t_no_nodin_swap')->result();
      $this->response($get_nodin, 200);
    }
}
