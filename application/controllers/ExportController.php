<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ExportController extends REST_Controller {

    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    public function index_get(){
        $ne_id = $this->get('ne_id');
        if ($ne_id != '') {
            $this->db->where('NE_ID', $ne_id);
        }
        $getnodin = $this->db->get('t_nodin_swap')->result();
        $this->response($getnodin, 200);
    }

    public function index_post()
    {

    }

    public function index_put()
    {

    }

}
